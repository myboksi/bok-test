package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.domain.TestClass;


public interface TestRespository extends JpaRepository<TestClass, Long> {
	List<TestClass> findByname(String name);
}
